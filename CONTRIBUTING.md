# Contributing

Improvements and enhancements are welcome! To keep the quality of the code high, please write sensible unit tests and fulfill all commands listed below before creating a pull request.

- Import sort: `poetry run isort .`
- Code formatter `poetry run black .`
- Linter: `poetry run pylint osm_wikidata_quality_checker/**/*.py`
- Static type check: `poetry run mypy -p osm_wikidata_quality_checker`
- Unit test: `poetry run pytest ./tests/*`

## Configurations for development

For the development phase there are some more configurations possible than documented in the README. You can define where the following folders shoud be expected by the tool (folders must exist). For development, you can define all variables in an `.env` file. Use `env.template` to get started.

| **Environment variable** | **Default value** | **Description**                           |
| ------------------------ | ----------------- | ----------------------------------------- |
| `DUMP_FOLDER`            | `./dumps`         | folder where the downloaded dumps end up. |
| `DB_FOLDER`              | `./db`            | folder where the databases are created.   |
| `REPORT_FOLDER`          | `./reports`       | folder where the reports are created.     |
| `LOG_FOLDER`             | `./logs`          | folder in which the logs are written.     |
