# OpenStreetMap Wikidata Quality Checker

## Introduction

The `osm-wikidata-quality-checker` is a [quality assurance tool](https://wiki.openstreetmap.org/wiki/Quality_assurance) for OpenStreetMap. It searches for invalid or inaccurate links between OpenStreetMap and Wikidata by evaluating [Wikidata tags](https://wiki.openstreetmap.org/wiki/Key:wikidata). The issues found are reported in different formats and can be sent to the [Osmose frontend](https://wiki.openstreetmap.org/wiki/Osmose). For more details, see the [System overview](### System overview) chapter.

The tool is written in Python and intended for use in a single Docker container.

## Table of Contents

[[_TOC_]]

## Installation

### Prerequisite and system requirements

Since the tool is dockered, your system must have Docker installed and be connected to the Internet for dump downloads.

Your system should have at least:

- 16 GB RAM
- 300 GB free ROM
- 4 physical or logical CPU cores

### Configuration

The tool can be configured by setting environment variables. Usually only the Osmose credentials need to be set. The others are useful for development or customization.

| **Environment variable** | **Default value**                                                       | **Description**                                                                   |
| ------------------------ | ----------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| `OSMOSE_SOURCE`          |                                                                         | Osmose frontend id of source (username). _Mandatory_                              |
| `OSMOSE_CODE`            |                                                                         | Osmose frontend password associated with the source. _Mandatory_                  |
| `OSMOSE_API_HOST`        | `https://osmose.openstreetmap.fr`                                       | Osmose frontend host. If left blank no report will be sent                        |
| `DO_DOWNLOAD`            | `osm,wd,redirects`                                                      | Specifies whether and which dumps the tool should download, disable all with `no` |
| `DO_IMPORT`              | `osm,wd,redirects`                                                      | Specifies which databases the tool should create, disable all with `no`           |
| `DO_CHECKS`              | `yes`                                                                   | Values 'no' does not execute any checks                                           |
| `OSM_DUMP_URL`           | `https://planet.openstreetmap.org/pbf/planet-latest.osm.pbf`            | URL of the OpenStreetMap dumps to download                                        |
| `WD_DUMP_URL`            | `https://dumps.wikimedia.org/wikidatawiki/entities/latest-all.json.bz2` | URL of the Wikidata dumps to download                                             |
| `WD_REDIRECTS_URL`       | `https://query.wikidata.org/sparql`                  | URL where to query redirects                                       |
| `WD_DUMP_FILENAME`       | `latest-all.json.bz2`                                                   | Filename of the download OpenStreetMap dump                                       |
| `OSM_DUMP_FILENAME`      | `planet-latest.osm.pbf`                                                 | Filename of the download Wikidata dump                                            |
| `WD_REDIRECTS_FILENAME`  | `qredirects.csv`                                                     | Filename of the download Wikidata redirects                                       |
| `OSM_DB_FILENAME`        | `osm.db`                                                                | Filename of the OpenStreetMap database                                            |
| `WD_DB_FILENAME`         | `wikidata.db`                                                           | Filename of the Wikidata database                                                 |
| `MP_CPU_THRESHOLD`       | `4`                                                                     | Threshold for the use of multiprocessing on checks                                |
| `WD_DUMP_MIN_ALLOWED_FILESIZE`       | `0` | The Wikidata dump file size must be larger than the value (bytes). This is a safety feature and will be checked before importing the dump |
| `OSM_DUMP_MIN_ALLOWED_FILESIZE`       | `0` | The OpenStreetMap dump file size must be larger than the value (bytes). This is a safety feature and will be checked before importing the dump |
| `WD_DB_MIN_ALLOWED_FILESIZE`       | `0` | The Wikidata db file size must be larger than the value (bytes). This is a safety feature and will be checked before checking |
| `OSM_DB_MIN_ALLOWED_FILESIZE`       | `0` | The OpenStreetMap db file size must be larger than the value (bytes). This is a safety feature and will be checked before checking |
| `ALERT_URL`       | `` | GitLab Alerts URL for sending status. If left blank no status will be sent  |
| `ALERT_AUTH`       | `` | GitLab Alerts authorization key  |
| `INSTANCE_NAME`       | `owqc` | An individual name that is logged and included in alerts to distinguish multiple instances |
| `PATH_2_OSMOSE_COUNTRY_CONFIG`       | `<app location>/res/osmose_country_config.json` | Path to Osmose Country config file. For debug and test purpose |
| `USE_FILE_IDX_CACHE`       | `yes` | Whetever to use ['flex_mem' index cache](https://docs.osmcode.org/pyosmium/latest/ref_osmium.html#osmium.SimpleHandler.apply_file) during osm import. If disabled less memory is used, but import takes longer. Recommended for debug and test purpose only |

## Run docker

Example of a minimal docker-compose file. The `outside_docker/` paths refer to a freely choosable directory on the system where outputs will be placed. The docker container will terminate after the script finishes.

```yml
version: "3"

services:
  osm-wikidata-quality-checker:
    image: registry.gitlab.com/geometalab/osm-wikidata-quality-checker:latest
    restart: "no"
    environment:
      OSMOSE_SOURCE: "username"
      OSMOSE_CODE: "password"
    volumes:
      - /outside_docker/dumps:/data/dumps
      - /outside_docker/db:/data/db
      - /outside_docker/reports:/data/reports
      - /outside_docker/logs:/data/logs
```

## Outputs

### Reports

The following files are generated and saved in the `reports` folder. They contain all found issues of a run:

- `<date_time>_report.csv`: CSV file which contains all issues
- `<date_time>_report.xlsx`: Excel file which contains all issues
- `<date_time>_osmose_report.xml`: XML file which is sent to the Osmose frontend

> **Important**: At the moment, non-area `OsmRealtion`'s have no location. Issues detected on such objects are not beeing reported to Osmsose until we have a reliable and fast way to get the location of relations that are not areas.

### Logs

Each run generates a log file inside of `logs` folder.

- `log_<date_time>.log`

### Other artefacts

The files in the folders `dumps` and `db` are files that are rewritten with each run. They can be deleted between runs without any consequences, but may help with debugging and development.

## How the tools works

### System overview

The graphic below gives an overview of the system and shows the data flow.

In a nutshell, the tool does the following:

1. Download dump files for OpenStreetMap and Wikidata.
2. Import relevant information from the dumps into local databases (indexes).
3. Perform checks on links from OSM to WD
4. Report the found issues to the Osmose frontend

```plantuml
@startuml
  skinparam component {
    BackgroundColor #EFEFEF
    BorderColor #4c66b5
    ArrowColor #4c66b5
  }

  skinparam interface {
    BackgroundColor #EFEFEF
    BorderColor #4c66b5
    ArrowColor #4c66b5
  }

package "osm wikidata quality checker" {
  [internet sources] -down-> [Downloader]
  [Downloader] -down-> [osm dump]
  [Downloader] -down-> [wd dump]
  [Downloader] -down-> [wd redirects]

  [osm dump] -down-> [OSM importer]
  [wd dump] -down-> [WD importer]
  [wd redirects] -down-> [Redirects importer]

  [OSM importer] -down-> [osm db]
  [WD importer] -down-> [wd db]
  [Redirects importer] -down-> [redirects db]

  [osm db] -down-> [Checker]
  [wd db] -down-> [Checker]
  [redirects db] -down-> [Checker]

  [Checker] -down-> [Reporter]
  [Reporter] -down-> [osmose_report.xml]
  [osmose_report.xml] -down-> [osmose-frontend]

  file "osm dump" {

  }

  file "wd dump" {

  }

  file "wd redirects" {

  }

  database "osm db" {
  }

  database "wd db" {
  }

  database "redirects db" {
  }

  file "osmose_report.xml" {
  }

}

cloud "internet sources" {
}

cloud "osmose-frontend" {
}

@enduml
```

### Basic checks

These checks are always done and ensure, that a valid and existing Wikidata tag is present.

| **Check name**           | **Description**                                                                                                                                                                                                                                   | **Issue class**             |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------- |
| `_is_valid_format`       | Checks if the value of any wikidata tag starts with letter Q followed by a integer (e.g. `Q123`). Multible values are allowed when seperated by a semicolon (e.g. `Q123;Q124`). Lexems (e.g. `L123`) are also a valid entry, but are not checked. | ` InvalidWdItemFormatIssue` |
| part of `check` function | Checks if the linked item exists on Wikidata.                                                                                                                                                                                                     | `NotExistingWdItemIssue`    |
| `_is_redirected`         | Checks if the wikidata item is redirected. It then offers a fix suggestion with the redirection target                                                                                                                                            | `RedirectedWdItemIssue`     |

### Link checks

After the basic checks, further checks are performed for each link. A link is a single connection between an OSM object and a wikidata element. For Example, an OSM node with the tags `wikidata=Q1;Q2` as well as `brand:wikidata=Q3` has 3 links.

The order of the checks is crucial. It stops at the first find, so at most one issue per link is generated.

| **Check name**                              | **Description**                                                                                                                                                                                                                                                                                                                                                                                           | **Issue class**                  |
| ------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| `check_living_organism`                     | It is not allowed to link wikidata tags `wikidata`, `brand:wikidata`, `operator:wikidata` (humans allowed), `network:wikidata` or `flag:wikidata` directly to a living organism (human, animal, plant). The check proivde fixes for suitable secondary Wikidata links (e.g. `subject:wikidata` or `buried:wikidata`).                                                                                     | `LivingOrganismIssue`            |
| `check_unpermitted_instance,`               | This check looks for links on Wikidata items with prohibited categories. These are `Wikimedia disambiguation page (Q4167410)`, `Wikimedia category (Q4167836)`, `Wikimedia list article (Q13406463)` and `list (Q12139612)` According to [Key:Wikidata](https://wiki.openstreetmap.org/wiki/Key:wikidata), these are not permitetted, as only items that relate directly to the feature should be linked. | `UnpermittedInstanceIssue`       |
| `check_primary_tag_instance_of`             | Checks if any tags matches with the claims of the linked Wikidata item. E.g. an osm-object with a `industrial=port` tag should be linked to an wikidata item which is an `instance of (P31)` or `subclass of (P279)` of `Port (Q44782)` (or more specific).                                                                                                                                               | `PrimaryTagClaimMismatchIssue`   |
| `check_secondary_tags_instance_of`          | If the OSM object has a [secondary Wikidata link](https://wiki.openstreetmap.org/wiki/Key:wikidata#Secondary_Wikidata_links), the prefix should match the category of the Wikidata item. E.g. `brand:wikidata=` should be linked to a `instance of (P31)` or `subclass of (P279)` of `brand (Q98)`, `organization (Q43229)` or `trademark (Q167270)` (or more specific).                                  | `SecondaryTagClaimMismatchIssue` |
| `check_places`                              | If an OSM object has a `place` tag, it checks if coordinates, name and postal_code match the linked Wikidata item. This is done by calculating a score over all existing features. (Currently, not all values for place are supported.)                                                                                                                                                                   | `PlaceMismatchIssue`             |
| `check_very_large_distance`                 | Checks if the coordinates in OpenStreetMap and Wikidata are very fare apart (>2000km), which is most likely incorrect.                                                                                                                                                                                                                                                                                    | `VeryLargeDistanceIssue`         |
| `check_replaced_wikidata_item` _(inactive)_ | Checks if the Wikidata item has a `replaced by (P1366)` property. It then offers a fix suggestion with the replaced target. The check is currently inactive due to lack of accuracy and could be improved.                                                                                                                                                                                                | `ReplacedWdItemIssue`            |
| `check_dissolved_wd_item` _(inactive)_      | Checks if the Wikidata item has a `dissolved, abolished or demolished date (P576)` property. The check is currently inactive due to lack of accuracy and could be improved.                                                                                                                                                                                                                               | `DissolvedWdItemIssue`           |

## Authors

- [Timon Erhart](https://gitlab.com/turbotimon)
- [Jari Elmer](https://gitlab.com/jarielmer)
