__version__ = "0.1.0"
import os
import sys

from .main import run

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

run()
