import json
import logging
import os
from abc import ABC, abstractmethod
from typing import Dict, List, Type

from sqlalchemy import create_engine, event
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.expression import Insert
from sqlalchemy.types import TEXT, TypeDecorator


class AbstractDatabase(ABC):
    def __init__(self, path_2_db: str):
        self.engine = create_engine(f"sqlite:///{path_2_db}", echo=False, future=True)

    def _init(self, base: Type):
        event.listen(self.engine, "connect", set_journal_mode)
        base.metadata.create_all(self.engine)

    @abstractmethod
    def add_all(self, objs: List):
        pass


@compiles(Insert, "sqlite")
def suffix_insert(insert, compiler, **kwargs):
    stmt = compiler.visit_insert(insert, **kwargs)
    if insert.dialect_kwargs.get("sqlite_on_conflict_do_nothing"):
        stmt += " ON CONFLICT DO NOTHING"
    return stmt


Insert.argument_for("sqlite", "on_conflict_do_nothing", True)


def set_journal_mode(dbapi_con, _con_record):
    dbapi_con.execute("PRAGMA journal_mode = OFF")
    dbapi_con.execute("PRAGMA locking_mode = EXCLUSIVE")


def remove_existing_database(path_2_db):
    if os.path.exists(path_2_db):
        logging.info("remove existing databases at %s", path_2_db)
        os.remove(path_2_db)


def do_databases_exist(database_paths: List[str]) -> bool:
    all_exist = True
    for path_2_database in database_paths:
        if not os.path.exists(path_2_database):
            logging.error("database '%s' does not exist", path_2_database)
            all_exist = False
    return all_exist


class JSONEncodedDict(TypeDecorator):
    "Represents an immutable structure as a json-encoded string."

    impl = TEXT
    python_type = Dict
    cache_ok = True

    def process_bind_param(self, value, dialect) -> str | None:
        if value is not None:
            new_value = json.dumps(value)
            return new_value
        return None

    def process_result_value(self, value: str, dialect) -> Dict | None:
        if value is not None:
            new_value = json.loads(value)
            return new_value
        return None

    def process_literal_param(self, value, dialect):
        pass


class JSONEncodedList(TypeDecorator):
    "Represents an immutable structure as a json-encoded string."

    impl = TEXT
    python_type = list
    cache_ok = True

    def process_bind_param(self, value, dialect) -> str | None:
        if value is not None:
            new_value = json.dumps(value)
            return new_value
        return None

    def process_result_value(self, value: str, dialect) -> list | None:
        if value is not None:
            new_value = json.loads(value)
            return new_value
        return None

    def process_literal_param(self, value, dialect):
        pass


class JSONEncodedSet(TypeDecorator):
    "Represents an immutable structure as a json-encoded string."

    impl = TEXT
    python_type = set
    cache_ok = True

    def process_bind_param(self, value, dialect) -> str | None:
        if value is not None:
            new_value = json.dumps(list(value))
            return new_value
        return None

    def process_result_value(self, value: str, dialect) -> set | None:
        if value is not None:
            new_value = set(json.loads(value))
            return new_value
        return None

    def process_literal_param(self, value, dialect):
        pass
