import requests
import tqdm


def download(url: str, path_2_file: str, progress_bar_pos: int):
    req = requests.get(url, stream=True)
    total_length = int(req.headers.get("content-length", 0))
    with open(path_2_file, "wb") as file, tqdm.tqdm(
        desc=url,
        total=total_length,
        unit="b",
        unit_scale=True,
        unit_divisor=1024,
        position=progress_bar_pos,
    ) as pbar:
        for chunk in req.iter_content(chunk_size=65536):
            if chunk:
                pbar.update(len(chunk))
                file.write(chunk)
