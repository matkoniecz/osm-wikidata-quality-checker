import enum
import logging
import socket

import requests

from .env import ALERT_AUTH, ALERT_URL, INSTANCE_NAME


class Severity(str, enum.Enum):
    Critical = "Critical"
    High = "High"
    Medium = "Medium"
    Low = "Low"
    Info = "Info"
    Unknown = "Unknown"


def send_alert(
    title: str,
    description: str,
    severity: Severity = Severity.Info,
    url=ALERT_URL,
    auth=ALERT_AUTH,
) -> int:
    if url:
        headers = {"Authorization": "Bearer " + auth}
        body = {
            "title": f"{INSTANCE_NAME}:{title}",
            "description": description,
            "severity": severity,
            "hosts": socket.gethostname(),
        }
        response = requests.post(url, json=body, headers=headers)
        return response.status_code
    return -1


def send_alert_with_logging(
    title: str,
    description: str,
    severity: Severity = Severity.Info,
    url=ALERT_URL,
    auth=ALERT_AUTH,
):
    if url:
        status_code = send_alert(title, description, severity, url=url, auth=auth)
        if status_code == 200:
            logging.info("alert sent: %s", title)
        else:
            logging.error("alert sent with error code %d: %s", status_code, title)
