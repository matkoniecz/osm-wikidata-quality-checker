import logging
import os
import sys
from datetime import datetime
from multiprocessing import JoinableQueue, Process
from typing import List, Type

import psutil

from . import env
from .checker import check, mp_checker
from .checker_issues import CheckerIssue
from .database_common import do_databases_exist, remove_existing_database
from .downloader import download
from .gitlab_alerts import Severity, send_alert_with_logging
from .osm_database import (
    OsmArea,
    OsmDatabase,
    OsmNode,
    OsmObject,
    OsmOsmoseCountry,
    OsmRelation,
    OsmWay,
)
from .osm_import import check_all_countries_imported, osm_import
from .osmose_reporter import OsmoseReporter
from .query_redirects import query_and_save_redirects
from .reporter import (
    CloseReport,
    CsvReporter,
    ExcelReporter,
    MultiReporter,
    mp_reporter,
)
from .wd_database import WdDatabase
from .wd_import import wd_import
from .wd_redirects_import import redirects_import

START_TIME = datetime.now().strftime("%Y-%m-%d_%H-%M")

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] [%(levelname)s] [%(processName)s] - %(message)s",
    handlers=[
        logging.FileHandler(f"{env.LOG_FOLDER}/log_{START_TIME}.log"),
        logging.StreamHandler(sys.stdout),
    ],
)


def _assert_min_allowed_filesize(path2file, min_allowed_filesize):
    filesize = os.stat(path2file).st_size
    if filesize < min_allowed_filesize:
        raise Exception(
            f"file size {path2file} is ({filesize}) smaller than allowed ({min_allowed_filesize})",
        )
    logging.info(
        "filesize check ok: %d >= %d for %s",
        filesize,
        min_allowed_filesize,
        path2file,
    )


def _import_all_mp(do_osm: bool, do_wd: bool, do_redirects: bool):
    logging.info(
        f"start import (osm={do_osm} / wd={do_wd}) / redirects={do_redirects})"
    )
    # Start sub-processes
    if do_osm:
        _assert_min_allowed_filesize(
            env.PATH_2_OSM_DUMP, env.OSM_DUMP_MIN_ALLOWED_FILESIZE
        )
        logging.info("start import osm dump")
        remove_existing_database(env.PATH_2_OSM_DB)
        osm_imp_proc = Process(
            target=osm_import,
            args=(env.PATH_2_OSM_DB, env.PATH_2_OSM_DUMP),
            kwargs={"use_file_idx_cache": env.USE_FILE_IDX_CACHE},
        )
        osm_imp_proc.start()

    if do_wd:
        _assert_min_allowed_filesize(
            env.PATH_2_WD_DUMP, env.WD_DUMP_MIN_ALLOWED_FILESIZE
        )
        logging.info("start import wd dump")
        remove_existing_database(env.PATH_2_WD_DB)
        wd_imp_proc = Process(
            target=wd_import,
            args=(env.PATH_2_WD_DB, env.PATH_2_WD_DUMP),
        )
        wd_imp_proc.start()

    if do_redirects:
        logging.info("start import wd redirects")
        remove_existing_database(env.PATH_2_WD_REDIRECTS_DB)
        redirects_imp_proc = Process(
            target=redirects_import,
            args=(
                env.PATH_2_WD_REDIRECTS_DB,
                env.PATH_2_WD_REDIRECTS,
            ),
        )
        redirects_imp_proc.start()

    # Join sub_processes
    if do_redirects:
        redirects_imp_proc.join()
        if (code := redirects_imp_proc.exitcode) != 0:
            raise Exception(f"redirects import failed with exitcode {code}")
        logging.info("end import for wd redirects")

    if do_wd:
        wd_imp_proc.join()
        if (code := wd_imp_proc.exitcode) != 0:
            raise Exception(f"wd import failed with exitcode {code}")
        logging.info("end import for wd dump")

    if do_osm:
        osm_imp_proc.join()
        if (code := osm_imp_proc.exitcode) != 0:
            raise Exception(f"osm import failed with exitcode {code}")
        logging.info("end import osm dump")

    logging.info("end import all")


def _get_multi_reporter(osmose_countries: List[OsmOsmoseCountry]):
    prefix = "report_" + START_TIME
    return MultiReporter(
        (
            CsvReporter(env.REPORT_FOLDER, prefix + ".csv"),
            OsmoseReporter(env.REPORT_FOLDER, osmose_countries),
            ExcelReporter(env.REPORT_FOLDER, prefix + ".xlsx", START_TIME),
        )
    )


# pylint: disable=consider-using-f-string
def _download_all_mp(do_osm: bool, do_wd: bool, do_wd_redirects: bool):
    logging.info("start download")
    download_processes: List[Process] = []
    i = 0
    if do_osm:
        p = Process(
            target=download,
            args=(env.OSM_DUMP_URL, env.PATH_2_OSM_DUMP, i),
        )
        p.start()
        download_processes.append(p)
        i += 1
    if do_wd:
        p = Process(
            target=download,
            args=(env.WD_DUMP_URL, env.PATH_2_WD_DUMP, i),
        )
        p.start()
        download_processes.append(p)
        i += 1
    if do_wd_redirects:
        p = Process(
            target=query_and_save_redirects,
            args=(env.WD_REDIRECTS_URL, env.PATH_2_WD_REDIRECTS, i),
        )
        p.start()
        download_processes.append(p)
        i += 1

    for p in download_processes:
        p.join()
        if (code := p.exitcode) != 0:
            raise Exception(f"a download failed with exitcode {code}")

    logging.info("end download")


def _check_all():
    _assert_min_allowed_filesize(env.PATH_2_OSM_DB, env.OSM_DB_MIN_ALLOWED_FILESIZE)
    _assert_min_allowed_filesize(env.PATH_2_WD_DB, env.WD_DB_MIN_ALLOWED_FILESIZE)
    logging.info("start check all")
    if not do_databases_exist(
        [env.PATH_2_OSM_DB, env.PATH_2_WD_DB, env.PATH_2_WD_REDIRECTS_DB]
    ):
        return
    osm_db = OsmDatabase(env.PATH_2_OSM_DB)
    wd_db = WdDatabase(env.PATH_2_WD_DB)
    wd_redirects_db = WdDatabase(env.PATH_2_WD_REDIRECTS_DB)
    reporter = _get_multi_reporter(osm_db.get_all_osmose_countries())
    issues: List[CheckerIssue] = []

    for osm_objs in osm_db.get_all():
        for osm_obj in osm_objs:
            if issue := check(osm_obj, wd_db, wd_redirects_db):
                issues.append(issue)
        reporter.report(issues)
        reporter.flush()
        issues = []
    reporter.close()
    logging.info("end check all")


def _osm_obj_queue_filler(
    path_2_osm_db: str, queue: JoinableQueue, osm_type: Type[OsmObject]
):
    osm_db = OsmDatabase(path_2_osm_db)
    for osm_objs in osm_db.get_all_type(osm_type):
        queue.put(osm_objs)


def _check_all_mp():
    _assert_min_allowed_filesize(env.PATH_2_OSM_DB, env.OSM_DB_MIN_ALLOWED_FILESIZE)
    _assert_min_allowed_filesize(env.PATH_2_WD_DB, env.WD_DB_MIN_ALLOWED_FILESIZE)
    logging.info("start check all (mp)")
    if not do_databases_exist(
        [env.PATH_2_OSM_DB, env.PATH_2_WD_DB, env.PATH_2_WD_REDIRECTS_DB]
    ):
        return
    osm_obj_queue = JoinableQueue(100)
    issues_queue = JoinableQueue()
    check_process_count = psutil.cpu_count()
    osm_objs_processes: List[Process] = []
    checker_processes: List[Process] = []

    for osm_type in [OsmNode, OsmWay, OsmRelation, OsmArea]:
        process = Process(
            target=_osm_obj_queue_filler,
            args=(env.PATH_2_OSM_DB, osm_obj_queue, osm_type),
        )
        process.start()
        osm_objs_processes.append(process)

    for i in range(check_process_count):
        process = Process(
            target=mp_checker,
            args=(
                env.PATH_2_WD_DB,
                env.PATH_2_WD_REDIRECTS_DB,
                osm_obj_queue,
                issues_queue,
            ),
            name=f"checker_{i}",
        )
        process.daemon = True
        process.start()
        checker_processes.append(process)

    reporter = _get_multi_reporter(
        OsmDatabase(env.PATH_2_OSM_DB).get_all_osmose_countries()
    )
    reporter_processs = Process(target=mp_reporter, args=(reporter, issues_queue))
    reporter_processs.daemon = True
    reporter_processs.start()

    for process in osm_objs_processes:
        process.join()

    osm_obj_queue.join()
    issues_queue.join()

    for process in checker_processes:
        process.terminate()
        process.join()

    issues_queue.put(CloseReport())
    issues_queue.join()

    reporter_processs.terminate()
    reporter_processs.join()
    logging.info("end check all (mp)")


def _log_env_vars():
    for env_var, val in sorted(env.__dict__.items(), key=lambda kv: kv[0]):
        if env_var.isupper() and not env_var.startswith("_"):
            # hide secrets
            if env_var in ("ALERT_AUTH", "OSMOSE_CODE"):
                val = "<very-secret>" if val else "<empty-string>"
            logging.info("%s: '%s'", env_var, val)


def run():
    try:
        _log_env_vars()
        if any([env.DO_OSM_DOWNLOAD, env.DO_WD_DOWNLOAD, env.DO_WD_REDIRECTS_DOWNLOAD]):
            send_alert_with_logging("Start download", "")
            _download_all_mp(
                env.DO_OSM_DOWNLOAD, env.DO_WD_DOWNLOAD, env.DO_WD_REDIRECTS_DOWNLOAD
            )
        if any([env.DO_OSM_IMPORT, env.DO_WD_IMPORT, env.DO_WD_REDIRECTS_IMPORT]):
            send_alert_with_logging("Start import", "")
            _import_all_mp(
                env.DO_OSM_IMPORT, env.DO_WD_IMPORT, env.DO_WD_REDIRECTS_IMPORT
            )
        if env.DO_OSM_IMPORT or env.DO_CHECKS:
            check_all_countries_imported(env.PATH_2_OSM_DB)
        if env.DO_CHECKS:
            send_alert_with_logging("Start check", "")
            # run mp check on server
            if psutil.cpu_count() >= env.MP_CPU_THRESHOLD:
                _check_all_mp()
            else:
                _check_all()

        send_alert_with_logging("End run", "")
    except Exception as e:  # pylint: disable=broad-except
        send_alert_with_logging(
            f"Run terminated with {type(e).__name__}",
            str(e),
            severity=Severity.Critical,
        )
        logging.error("Run terminated with %s: %s", type(e).__name__, str(e))
        raise e  # to log traceback etc.
