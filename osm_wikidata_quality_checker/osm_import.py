import json
import logging
import tempfile
from multiprocessing import JoinableQueue, Process
from typing import Callable, Dict, List, Tuple, Type

from osmium import SimpleHandler, geom

from .env import PATH_2_OSMOSE_COUNTRY_CONFIG
from .import_common import (
    DEFAULT_BULK_SIZE,
    DEFAULT_QUEUE_MAX,
    InsertBuffer,
    queue_to_database,
)
from .osm_database import (
    OsmArea,
    OsmDatabase,
    OsmNode,
    OsmOsmoseCountry,
    OsmRelation,
    OsmWay,
)

OsmoseCountryConfig = Dict[int, object]


class _OsmDumpPbfReader(SimpleHandler):
    def __init__(
        self,
        insert_function: Callable,
    ):
        SimpleHandler.__init__(self)
        self.insert_function = insert_function


def check_all_countries_imported(path_2_db: str, osmose_country_config=None):
    if osmose_country_config is None:
        osmose_country_config = _build_osmose_country_config()
    db = OsmDatabase(path_2_db)
    country_oid_in_db = {c.oid for c in db.get_all_osmose_countries()}
    _check_all_countries_imported(country_oid_in_db, osmose_country_config)


def _check_all_countries_imported(country_oid_in_db, osmose_country_config):
    all_good = True
    for oid, config in osmose_country_config.items():
        if oid not in country_oid_in_db:
            logging.warning(
                "osmose country %s (rel=%d / country_code=%s) is not in db",
                config["name"],
                oid,
                config["country_code"],
            )
            all_good = False
    if all_good:
        logging.info("all %d osmose countries in db", len(osmose_country_config))


def _build_osmose_country_config(
    path: str = PATH_2_OSMOSE_COUNTRY_CONFIG,
) -> OsmoseCountryConfig:
    osmose_country_config = {}
    with open(
        path,
        "r",
        encoding="utf8",
    ) as f:
        json_dict: Dict[str, object] = json.load(f)
        osmose_country_config = {int(key): value for key, value in json_dict.items()}
    return osmose_country_config


class _OsmDumpPbfReaderWayArea(_OsmDumpPbfReader):
    def __init__(
        self,
        insert_function: Callable,
        osmose_country_config: OsmoseCountryConfig,
    ):
        super().__init__(insert_function)
        self.wkbfab = geom.WKBFactory()
        self.osmose_country_config = osmose_country_config

    def area(self, area):
        wkb = None
        if "wikidata" in "".join([t.k for t in area.tags]):
            tags = {t.k: t.v for t in area.tags}

            num_rings_outer, num_rings_inner = area.num_rings()
            # Ausschliessen wenn: num_rings_inner==0 && num_rings_outer==0
            # Da es sonst ein Import-Error gibt
            if num_rings_inner == 0 and num_rings_outer == 0:
                return

            wkb = self._create_area_wkb(area)

            osm_obj = OsmArea()
            osm_obj.oid = area.orig_id()
            osm_obj.uid = area.id
            osm_obj.tags = tags
            osm_obj.user = area.user if len(area.user) > 0 else None
            osm_obj.version = area.version
            osm_obj.is_way = area.from_way()
            osm_obj.is_multipolygon = area.is_multipolygon()
            osm_obj.num_rings_inner = num_rings_inner
            osm_obj.num_rings_outer = num_rings_outer
            osm_obj._wkb_hex = wkb  # pylint: disable=protected-access

            self.insert_function(osm_obj)

        if not area.from_way() and area.orig_id() in self.osmose_country_config:
            oid = area.orig_id()
            # get and remove country (so list is empty if every country is found)
            config = self.osmose_country_config[oid]
            osmose_country = OsmOsmoseCountry()
            osmose_country.oid = oid
            osmose_country.name = config["name"]
            osmose_country.country_code = config["country_code"]
            osmose_country.is_multipolygon = area.is_multipolygon()
            if wkb is None:
                wkb = self._create_area_wkb(area)
            osmose_country._wkb_hex = wkb  # pylint: disable=protected-access
            self.insert_function(osmose_country)

    def _create_area_wkb(self, area):
        wkb = None
        try:
            wkb = self.wkbfab.create_multipolygon(area)
        except Exception as ex:  # pylint: disable=broad-except
            logging.error(
                "can not create multipolygon wkb for area odi %s, id %s, exception: %s",
                area.orig_id(),
                area.id,
                ex,
            )
        return wkb

    def way(self, way):
        if "wikidata" in "".join([t.k for t in way.tags]):
            tags = {t.k: t.v for t in way.tags}
            is_closed = way.is_closed()
            # if area is closed, and area-tag is not "no", the way will be handled as area
            if is_closed and tags.get("area", "yes") != "no":
                return

            wkb = None
            try:
                wkb = self.wkbfab.create_linestring(way)
            except Exception as ex:  # pylint: disable=broad-except
                logging.error(
                    "can not create linestring wkb for way id %s, exception: %s",
                    way.id,
                    ex,
                )

            osm_obj = OsmWay()
            osm_obj.oid = way.id
            osm_obj.tags = tags
            osm_obj.user = way.user if len(way.user) > 0 else None
            osm_obj.version = way.version
            osm_obj.is_closed = is_closed
            osm_obj._wkb_hex = wkb  # pylint: disable=protected-access

            self.insert_function(osm_obj)


class _OsmDumpPbfReaderNode(_OsmDumpPbfReader):
    def node(self, node):
        if "wikidata" in "".join([t.k for t in node.tags]):
            tags = {t.k: t.v for t in node.tags}
            osm_obj = OsmNode()
            osm_obj.oid = node.id
            osm_obj.tags = tags
            osm_obj.user = node.user if len(node.user) > 0 else None
            osm_obj.version = node.version
            osm_obj.set_persistant_location(node.location.lat, node.location.lon)

            self.insert_function(osm_obj)


class _OsmDumpPbfReaderRelation(_OsmDumpPbfReader):
    def relation(self, relation):
        # if type is 'boundary' or 'multipolygon', then the relation will be handled as area
        if relation.tags.get("type") in {"boundary", "multipolygon"}:
            return

        if "wikidata" in "".join([t.k for t in relation.tags]):
            tags = {t.k: t.v for t in relation.tags}
            members = [(r.ref, r.role, r.type) for r in relation.members]

            osm_obj = OsmRelation()
            osm_obj.oid = relation.id
            osm_obj.tags = tags
            osm_obj.user = relation.user if len(relation.user) > 0 else None
            osm_obj.version = relation.version
            osm_obj.members = members
            self.insert_function(osm_obj)


class _OsmDumpPbfReaderAll(
    _OsmDumpPbfReaderNode,
    _OsmDumpPbfReaderRelation,
    _OsmDumpPbfReaderWayArea,
):  # pylint: disable=too-many-ancestors
    pass


def _get_index_cache(use_file=False) -> str:
    # info https://osmcode.org/osmium-concepts/#indexes
    if use_file:
        cache_file = tempfile.NamedTemporaryFile()
        return "dense_file_array," + cache_file.name

    return "flex_mem"


def _import_osm_type(
    path_2_osm_dump,
    osm_pbf_reader: Type[_OsmDumpPbfReader | _OsmDumpPbfReaderWayArea],
    idx: str | None,
    osmose_country_config: OsmoseCountryConfig | None,
    queue: JoinableQueue,
    bulk_size: int = DEFAULT_BULK_SIZE,
):
    buffer: InsertBuffer = InsertBuffer(queue.put, bulk_size)
    reader = None
    # HINT actualy type determination is done with 'is', the 'isinstnace' is only for mypy
    if osm_pbf_reader is _OsmDumpPbfReaderWayArea and isinstance(
        osm_pbf_reader, type(_OsmDumpPbfReaderWayArea)
    ):
        assert osmose_country_config is not None
        # cast(osmose_country_config, OsmoseCountryConfig)
        reader = osm_pbf_reader(
            buffer.insert, osmose_country_config=osmose_country_config
        )
    elif isinstance(osm_pbf_reader, type(_OsmDumpPbfReader)):
        reader = osm_pbf_reader(buffer.insert)
    assert reader is not None
    reader.apply_file(path_2_osm_dump, idx=idx if idx else "flex_mem")
    buffer.flush()


def osm_import(
    path_2_db: str,
    path_2_dump: str,
    bulk_size: int = DEFAULT_BULK_SIZE,
    use_multiprocessing: bool = True,
    use_file_idx_cache: bool = True,
):
    if use_multiprocessing:
        queue: JoinableQueue = JoinableQueue(maxsize=DEFAULT_QUEUE_MAX)
        db_process = Process(
            target=queue_to_database, args=(queue, OsmDatabase, path_2_db), daemon=True
        )
        db_process.start()

        processes: List[Process] = []
        osmose_country_config = _build_osmose_country_config()
        types: List[
            Tuple[Type[_OsmDumpPbfReader], str | None, OsmoseCountryConfig | None]
        ] = [
            (_OsmDumpPbfReaderNode, None, None),
            (_OsmDumpPbfReaderRelation, None, None),
            (
                _OsmDumpPbfReaderWayArea,
                _get_index_cache(use_file_idx_cache),
                osmose_country_config,
            ),
        ]

        for t in types:
            process = Process(
                target=_import_osm_type,
                args=(path_2_dump, t[0], t[1], t[2], queue, bulk_size),
            )
            process.start()
            processes.append(process)

        for p in processes:
            p.join()
            if (code := p.exitcode) != 0:
                raise Exception(
                    f"subprocess osm dump reader failed with exitcode {code}"
                )

        queue.join()
        db_process.terminate()
        db_process.join()
        if (code := db_process.exitcode) != -15:  # SIGTERM due to terminate()
            raise Exception(f"subprocess db_inserter failed with exitcode {code}")

    else:
        database = OsmDatabase(path_2_db)
        buffer: InsertBuffer = InsertBuffer(database.add_all, bulk_size)
        osmose_country_config = _build_osmose_country_config()
        reader = _OsmDumpPbfReaderAll(
            buffer.insert, osmose_country_config=osmose_country_config
        )
        reader.apply_file(path_2_dump, idx=_get_index_cache(use_file_idx_cache))
        buffer.flush()
