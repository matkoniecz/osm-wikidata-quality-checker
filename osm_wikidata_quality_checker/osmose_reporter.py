import logging
import os
from dataclasses import dataclass, field
from datetime import datetime
from io import TextIOWrapper
from pathlib import Path
from typing import Dict, Iterable, List, Type
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement

import requests
from shapely.geometry import Point, Polygon

from .checker_issues import *  # pylint: disable=wildcard-import,W0614
from .env import OSMOSE_API_HOST, OSMOSE_CODE, OSMOSE_SOURCE
from .osm_database import OsmOsmoseCountry
from .reporter import Reporter


@dataclass(frozen=True)
class OsmoseErrorDetails:
    # for more details see https://github.com/osm-fr/osmose-backend/blob/master/doc/0-Index.md#classes-of-issues
    id: int
    title: str
    detail: str
    fix: str | None = None
    trap: str | None = None
    example: str | None = None
    tag: str = "wikidata, tag, fixme"
    item: int = 3031
    level: int = 2
    source: str = "https://gitlab.com/geometalab/osm-wikidata-quality-checker"


_ISSUE_2_OSMOSE_CLASS_MAPPING: Dict[Type[CheckerIssue], OsmoseErrorDetails] = {
    InvalidWdItemFormatIssue: OsmoseErrorDetails(
        id=1,
        title="Incorrect value for Wikidata-Tag",
        detail="The format of the Wikidata-ID is wrong. Only valid values like `Q123` oder multiple items like `Q123;Q124` are allowed. Lexems like `L123` are also allowed for tags with `*:etymology:wikidata`, but ignored. See [Wikidata](https://wiki.openstreetmap.org/wiki/Wikidata) and [Key:wikidata](https://wiki.openstreetmap.org/wiki/Key:wikidata) for more information.",
        fix="If you see a wrongly formatted value, check if it matches the OSM object and correct it. Otherwise delete the tag.",
    ),
    NotExistingWdItemIssue: OsmoseErrorDetails(
        id=2,
        title="Wikidata item does not exist",
        detail="The linked Wikidata item was not found in the Wikidata database dump.",
        fix="Check if the tag realy does not exists, then delete the affected tag.",
        trap="We used the Wikidata database dump to check for this error. It is possible that there are Items which are not yet in the used dump, therefore false errors can be generated for very new items. Always make sure that it realy does not exist.",
    ),
    RedirectedWdItemIssue: OsmoseErrorDetails(
        id=3,
        title="Redirected value for Wikidata tag",
        detail="This Wikidata item is a redirect, you shoud change the QID to the redirect destionation.",
        fix="Change the value of the affected tag to the suggested new Wikidata-ID.",
    ),
    VeryLargeDistanceIssue: OsmoseErrorDetails(
        id=4,
        title="The distance between OSM object and linked Wikidata item is unusually large",
        detail="The shortest distance between the OSM object and the linked Wikidata item is very large, this hints to a incorectly linked Wikidata item.",
        fix="Verify that the link is in fact incorrect, then correct or delete the tag. It could also be that the qualifier is set wrongly, so that a prefix like `brand:wikidata`, `subject:wikidata` or `is_in:wikidata` fixes the problem. For more details on qualifier see [Secondary Wikidata links](https://wiki.openstreetmap.org/wiki/Key:wikidata#Secondary_Wikidata_links)",
    ),
    PrimaryTagClaimMismatchIssue: OsmoseErrorDetails(
        id=5,
        title="Characteristics of the OSM tags and linked Wikidata item do not match",
        detail="OSM tags do not match the `instance-of` and/or `subclass-of` claims in the Wikidata item.",
        example="An OSM object with tag `railway=*` should be an instance or subclass of `Transport infrastructure (Q350783)`. Or the operator of something (e.g. industrial factory) is specified in the primary wikidata tag (`wikidata=`), but should be specified as a secondary wikidata tag (`operator:wikidata=`).",
        fix="Either remove the tag or correct it by linking it to an matching Wikidata item. It could also be that you should use a secondary Wikidata link instead. Sometimes, however, the information on Wikidata is incomplete and should be corrected there.",
    ),
    SecondaryTagClaimMismatchIssue: OsmoseErrorDetails(
        id=6,
        title="The secondary Wikidata tag and the linked Wikidata item do not match",
        detail="The Wikidata tag prefix does not match the `instance-of` and/or `subclass-of` claims in the Wikidata item. Fore more details see [Secondary Wikidata links](https://wiki.openstreetmap.org/wiki/Key:wikidata#Secondary_Wikidata_links).",
        example="The secondary Wikidata tag `brand:wikidata` should be a instance or subclass of `Brand (Q431289)`, `Trademark (Q167270)` or `Organisation (Q43229)`.",
        fix="Remove the affected tag or change to another secondary Wikidata tag, for example use `brand:wikidata` instead of `operator:wikidata`. Or add the required `instance-of` or `subclass-of` claims to the Wikidata item.",
    ),
    UnpermittedInstanceIssue: OsmoseErrorDetails(
        id=7,
        title="The OSM object is linked to an unpermitted Wikidata item",
        detail="Only items that directly relate to the feature should be linked, do not link to Wikidata internals, lists or discussion pages. See [Key:Wikidata](https://wiki.openstreetmap.org/wiki/Key:wikidata) for more details.",
        example="A specific statue is linked to an Wikidata item representing a list of statues.",
        fix="Remove the incorect tag (or change it to the specific Wikidata item).",
    ),
    LivingOrganismIssue: OsmoseErrorDetails(
        id=8,
        title="Unpermitted link to an instance of living organism on Wikidata",
        detail="It is not allowed to link wikidata tags `wikidata`, `brand:wikidata`, `operator:wikidata` (humans allowed), `network:wikidata` or `flag:wikidata` directly to a living organism (human, animal, plant). Use a suitable secondary Wikidata link for this purpose. For more details see [Key:Wikidata](https://wiki.openstreetmap.org/wiki/Key:wikidata).",
        example="The name origin of a street name is marked with the tag `wikidata` instead of `name:etymology:wikidata`. The Human buried at a burial site is tag with the `wikidata` tag instead of `buried:wikidata`.",
        fix="Remove the incorect tag and/or apply one of the suggested tags.",
    ),
    PlaceMismatchIssue: OsmoseErrorDetails(
        id=9,
        title="The OSM object does not match the Wikidata item",
        detail="The name and/or distance and/or postal code found in the OSM tag and Wikidata claims do not match sufficiently.",
        fix="Verfy that the link is in fact incorrect, then correct or delete the tag.",
        trap="Sometimes claims on Wikidata, like coordinates, are just too inaccurate.",
    ),
    ReplacedWdItemIssue: OsmoseErrorDetails(
        id=10,
        title="Linked Wikidata item is replaced by other item",
        detail="This Wikidata item is replaced by another Wikidata item, you shoud check the validity of this Wikidata item and consider to change the QID to the 'replaced by' destionation.",
        fix="Check if this Wikidata item is still valid, if it is actually replaced by the other Wikidata item, set the new item as Value of the affected tag.",
    ),
    DissolvedWdItemIssue: OsmoseErrorDetails(
        id=11,
        title="Linked Wikidata item is dissolved, abolished or demolished",
        detail="Wikidata item has a 'dissolved, abolished or demolished date' and should therefore not exist anymore.",
        fix="Check if the real Thing behind the Wikidata item does in fact no longer exist. If so, remove this Wikidata Link from OSM.",
    ),
}


def _get_osmose_class(issue: CheckerIssue) -> OsmoseErrorDetails:
    return _ISSUE_2_OSMOSE_CLASS_MAPPING[type(issue)]


@dataclass
class _CountryAnalyser:
    relation_id: int
    country_code: str
    name: str
    poly: Polygon
    issues: List[CheckerIssue] = field(default_factory=list)


def _build_country_analysers(
    countries: List[OsmOsmoseCountry],
) -> List[_CountryAnalyser]:
    analysers = []
    for country in countries:
        poly = country.get_shape()
        a = _CountryAnalyser(country.oid, country.country_code, country.name, poly)
        analysers.append(a)

    return analysers


class OsmoseReporter(Reporter):
    def __init__(
        self,
        path: Path,
        countries: List[OsmOsmoseCountry],
        api_host: str = OSMOSE_API_HOST,
        api_source: str = OSMOSE_SOURCE,
        api_code: str = OSMOSE_CODE,
    ):
        logging.info("start Osmose report")
        time = datetime.now()
        self.timestamp = time.strftime("%Y-%m-%dT%H:%M:%SZ")
        self.timestamp_path = time.strftime("%Y-%m-%d_%H-%M")
        self.path_2_reports_folder = os.path.join(path, self.timestamp_path)
        os.makedirs(self.path_2_reports_folder)
        self.api_host: str = api_host
        self.api_source: str = api_source
        self.api_code: str = api_code
        self.fallback_issues: List[CheckerIssue] = []
        self._report_file: TextIOWrapper | None = None
        self._country_analysers: List[_CountryAnalyser] = _build_country_analysers(
            countries
        )

    def open(self):
        pass

    def report(self, issues: List[CheckerIssue]):
        for issue in issues:
            self._add_to_country_analyser(issue)

    def _add_to_country_analyser(self, issue: CheckerIssue):
        issue_location = issue.osm.get_location()

        if not issue_location:
            # HACK currently, there are no locations for (non-area) relations
            if not isinstance(issue.osm, OsmRelation):
                logging.error(
                    "no location for issue subclass: %s osm_type: %s osm_id: %s tag: %s",
                    issue.hash_id,
                    issue.osm.get_osm_type(),
                    issue.osm.oid,
                    issue.key,
                )
            return

        location_point = Point(issue_location.lon, issue_location.lat)
        for analyser in self._country_analysers:
            if location_point.within(analyser.poly) or location_point.touches(
                analyser.poly
            ):
                analyser.issues.append(issue)
                return

        # no country analyser found, add to fallback list
        logging.warning(
            "no country analyser found for issue with hash %s, osm: %s, location: %s",
            issue.hash_id,
            issue.osm,
            issue_location,
        )
        self.fallback_issues.append(issue)

    def flush(self):
        pass

    def close(self):
        if self._send_reports():
            logging.info("end sending reporter to Osmose")
        else:
            logging.error("one or more reports could not be sended to Osmose")
        logging.info("end Osmose reporter")

    def _send_reports(self) -> bool:
        if not self.api_host:
            logging.info("no xml report sent to Osmose (empty api_host)")
            return True
        logging.info("start sending reports to Osmose")
        all_ok = True
        for country_anaylser in self._country_analysers:
            country = country_anaylser.name
            path_2_report = os.path.join(
                self.path_2_reports_folder,
                f"report_{country}_{self.timestamp_path}.xml",
            )
            self._save_country_report(path_2_report, country_anaylser)
            all_ok = self._send_report(country, path_2_report) and all_ok

        return all_ok

    def _send_report(self, country, path_2_report) -> bool:
        # logging.info("send xml report to Osmose for country %s", country)
        req = None
        url = f"{self.api_host}/control/send-update"
        with open(path_2_report, "rb") as report_file:
            files = {
                "analyser": (None, self.api_source),
                "country": (None, country),
                "code": (None, self.api_code),
                "content": (path_2_report, report_file),
            }
            try:
                req = requests.post(url, files=files)
                if not req.ok:
                    logging.error(
                        "send xml report to Osmose for country %s failed with return status code %s (%s) and message: %s",
                        country,
                        url,
                        req.status_code,
                        req.reason,
                    )
                return req.ok
            except Exception as e:  # pylint: disable=broad-except
                logging.error(
                    "send xml report to Osmose for country %s failed due to %s: %s",
                    country,
                    type(e).__name__,
                    str(e),
                )
                return False

    def _save_country_report(self, path_2_file, country_analyser: _CountryAnalyser):
        with open(path_2_file, "w+", newline="", encoding="US-ASCII") as report_file:
            report_file.write(self._build_xml(country_analyser))
            report_file.flush()

    def _build_xml(
        self,
        country_analyser: _CountryAnalyser,
    ) -> str:
        timestamp_attr = {"timestamp": self.timestamp}
        root = Element("analysers", attrib=timestamp_attr)

        root.append(self._build_analyser(country_analyser.issues))

        return ET.tostring(
            root, encoding="US-ASCII", method="xml", xml_declaration=True
        ).decode()

    def _build_analyser(self, issues: List[CheckerIssue]):
        analyser = Element(
            "analyser",
            attrib={"timestamp": self.timestamp},
        )
        analyser.extend(self._build_classes())
        analyser.extend(self._build_errors(issues))
        return analyser

    @staticmethod
    def _build_classes() -> List[Element]:
        """
        Class definition (many)
        id: referenced by <error class="" />
        item: osmose frontend target item https://github.com/osm-fr/osmose-frontend/blob/759c208d180ae0ccca242e1559191f94ef8cadd4/tools/database/items_menu.txt
        level: matter of issue in OSM data, from "1" as major to "3" as minor
        tag: key-words of issue (not a set of OSM tags) https://osmose.openstreetmap.fr/api/0.3/tags
        title of class, one line per language
        """

        # used_classes = {_get_osmose_class(issue) for issue in issues}
        used_classes = _ISSUE_2_OSMOSE_CLASS_MAPPING.values()
        classes = []
        for cls in used_classes:
            class_elem = Element(
                "class",
                attrib={
                    "item": str(cls.item),
                    "tag": cls.tag,
                    "id": str(cls.id),
                    "level": str(cls.level),
                    "source": cls.source,
                },
            )
            SubElement(
                class_elem,
                "classtext",
                attrib={
                    "lang": "en",
                    "title": cls.title,
                },
            )

            further_class_info = [
                ("detail", cls.detail),
                ("fix", cls.fix),
                ("trap", cls.trap),
                ("example", cls.example),
            ]

            for element, value in further_class_info:
                if value:
                    SubElement(
                        class_elem,
                        element,
                        attrib={
                            "lang": "en",
                            "title": value,
                        },
                    )

            classes.append(class_elem)

        return classes

    @staticmethod
    def _get_fix(fix: AllFixTypes, osm_type: str, oid: int) -> Element:
        fix_elem = Element("fix")

        osm_elem = SubElement(fix_elem, osm_type, attrib={"id": str(oid)})

        fixes = [fix] if isinstance(fix, Fix) else fix.fixes
        for f in fixes:
            tag_elem_attrib = {"k": f.key, "action": "delete"}

            if isinstance(f, ValueFix):
                tag_elem_attrib["v"] = f.value
                if isinstance(f, ChangeFix):
                    tag_elem_attrib["action"] = "modify"
                else:
                    tag_elem_attrib["action"] = "create"

            SubElement(osm_elem, "tag", attrib=tag_elem_attrib)

        return fix_elem

    def _build_errors(self, issues: Iterable[CheckerIssue]) -> List[Element]:
        """
        Issue entry (many)
        class: refer to <class id="" />
        subclass: group the same kind of issue (optional)
        location: Issue location marker
        node: erroneous object (none or many), all details are optional
        text: optional subtitle, one line per language
        """
        errors: List[Element] = []
        for issue in issues:
            cls = _get_osmose_class(issue)
            osm_type = issue.osm.get_osm_type()

            issue_location = issue.osm.get_location()

            error = Element(
                "error", attrib={"class": str(cls.id), "subclass": str(issue.hash_id)}
            )

            if issue_location:
                SubElement(
                    error,
                    "location",
                    attrib={
                        "lat": str(issue_location.lat),
                        "lon": str(issue_location.lon),
                    },
                )

            error_obj_type = SubElement(
                error,
                osm_type,
                attrib={
                    "id": str(issue.osm.oid),
                    "user": str(issue.osm.user),
                    "version": str(issue.osm.version),
                    "lat": str(issue_location.lat) if issue_location else "",
                    "lon": str(issue_location.lon) if issue_location else "",
                },
            )

            SubElement(
                error_obj_type,
                "tag",
                attrib={"k": issue.key, "v": issue.osm.tags.get(issue.key, "")},
            )

            if issue.text:
                SubElement(
                    error,
                    "text",
                    attrib={
                        "lang": "en",
                        "value": issue.text,
                    },
                )

            if len(issue.fixes) > 0:
                fixes = SubElement(error, "fixes")
                for fix in issue.fixes:
                    fixes.append(self._get_fix(fix, osm_type, issue.osm.oid))

            errors.append(error)
        return errors
