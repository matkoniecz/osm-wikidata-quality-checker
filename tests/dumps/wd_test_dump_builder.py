import bz2
import json

# from ...osm_wikidata_quality_checker.env import PATH_2_WD_DUMP
# import osm_wikidata_quality_checker.env as env

path_2_wd_dump = "./latest-all.json.bz2"
path_2_output_dump = "./wd_test_dump.json.bz2"
nof_items = 200


def extract_dump(path_2_wd_dump: str, path_2_output_dump: str, nof_items: int):
    with bz2.open(path_2_output_dump, mode="wb") as output:
        with bz2.open(path_2_wd_dump) as input:
            c = 0
            for line in input:
                if c >= nof_items:
                    break

                # print(line)

                if len(line) <= 2:
                    output.write(line)
                    continue

                # Strip ',' character at end of line (line[-1] is newline)
                line_ = line
                if line[-2] == 44:
                    line_ = line[:-2]

                wd_item = json.loads(line_)
                if wd_item["id"][:1] != "Q":
                    continue

                output.write(line)
                c += 1


extract_dump(path_2_wd_dump, path_2_output_dump, nof_items)
