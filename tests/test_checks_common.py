import pytest
from shapely.geometry import Point

from osm_wikidata_quality_checker.checker_issues import (
    AreaLink,
    NodeLink,
    RelationLink,
    WayLink,
)
from osm_wikidata_quality_checker.checks_common import (
    _best_strings_ratio,
    _haversine_distance_km,
    _string_token_set_ratio,
    min_distance_km,
    score_names,
    score_postal_code,
)
from osm_wikidata_quality_checker.osm_database import (
    Location,
    OsmArea,
    OsmNode,
    OsmRelation,
    OsmWay,
)
from osm_wikidata_quality_checker.wd_database import WdCoordinate, WdItem


def test__best_string_ratio():
    assert _best_strings_ratio([], ["bla"]) is None
    assert _best_strings_ratio(["bla"], []) is None
    assert _best_strings_ratio(["bla"], ["bla"]) == pytest.approx(1.0)
    assert _best_strings_ratio(["bla", "yyy"], ["bla", "xxx"]) == pytest.approx(1.0)
    assert _best_strings_ratio(["yyy"], ["xxx"]) == pytest.approx(0.0)


def test__string_token_set_ratio():
    assert _string_token_set_ratio("", "bla") is None
    assert _string_token_set_ratio("bla", "") is None
    assert _string_token_set_ratio("bla", "bla") == pytest.approx(1.0)
    assert _string_token_set_ratio("bla yyy", "bla xxx") == pytest.approx(0.6)
    assert _string_token_set_ratio("bla yyy", "xxx bla") == pytest.approx(0.6)
    assert _string_token_set_ratio("yyy", "xxx") == pytest.approx(0.0)


def test__haversine_distance_km():
    assert _haversine_distance_km(0.0, 0.0, 1.0, 1.0) == pytest.approx(157.25)
    assert _haversine_distance_km(60.0, 6.0, 61.0, 6.1) == pytest.approx(111.33)


def test_min_distance_km():
    loc = Location(0.0, 0.0)
    shape = Point(0.0, 0.0)

    node = OsmNode()
    node._location = loc

    rel = OsmRelation()
    rel._location = loc

    area = OsmArea()
    area._shape = shape
    area._shape_created = True

    way = OsmWay()
    way._shape = shape
    way._shape_created = True

    rel = OsmRelation()
    rel._location = loc

    wd = WdItem()
    wd.coordinates = [WdCoordinate(1.0, 1.0)]

    expected = pytest.approx(
        157.2, 0.1
    )  # https://www.movable-type.co.uk/scripts/latlong.html

    assert min_distance_km(NodeLink(node, wd, "")) == expected
    assert min_distance_km(RelationLink(rel, wd, "")) == expected
    assert min_distance_km(AreaLink(area, wd, "")) == expected
    assert min_distance_km(WayLink(way, wd, "")) == expected


def test_score_postal_code(osmNode: OsmNode, wdItem: WdItem):
    link = NodeLink(osmNode, wdItem, "wikidata")

    osmNode.tags["postal_code"] = "12345"
    wdItem.postal_codes = ["12345"]
    assert score_postal_code(link) == pytest.approx(1.0)

    osmNode.tags["postal_code"] = "97-532"
    wdItem.postal_codes = ["97-515"]
    assert score_postal_code(link) == pytest.approx(0.67)

    osmNode.tags["postal_code"] = "123"
    wdItem.postal_codes = ["789"]
    assert score_postal_code(link) == pytest.approx(0.0)

    osmNode.tags["postal_code"] = "123"
    wdItem.postal_codes = ["123", "789"]
    assert score_postal_code(link) == pytest.approx(1.0)


def test_score_names(osmNode: OsmNode, wdItem: WdItem):
    link = NodeLink(osmNode, wdItem, "wikidata")

    osmNode.tags["name"] = "Pfäffikon"
    wdItem.names = {"en": "Pfäffikon"}
    assert score_names(link) == pytest.approx(1.0)

    osmNode.tags["name"] = "Pfäffikon"
    wdItem.names = {"en": "Wädenswil"}
    assert score_names(link) == pytest.approx(0.22)

    osmNode.tags["name"] = "Pfäffikon"
    wdItem.names = {"en": "Buchs"}
    assert score_names(link) == pytest.approx(0.0)

    osmNode.tags["name"] = "Pfäffikon"
    wdItem.names = {"en": "Pfäffikon (ZH)"}
    assert score_names(link) == pytest.approx(1.0)

    osmNode.tags["name"] = "Pfäffikon"
    wdItem.names = {"en": "Pfäffikon, ZH"}
    assert score_names(link) == pytest.approx(1.0)

    osmNode.tags["name"] = "Pfäffikon"
    wdItem.names = {"en": "Pfäffikon", "de": "Pfäffikon ZH"}
    assert score_names(link) == pytest.approx(1.0)

    osmNode.tags["name"] = "XXX"
    osmNode.tags["old_name"] = "Pfäffikon"
    wdItem.names = {"en": "Pfäffikon"}
    assert score_names(link) == pytest.approx(1.0)
