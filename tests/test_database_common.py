import os

from osm_wikidata_quality_checker.database_common import (
    do_databases_exist,
    remove_existing_database,
)
from osm_wikidata_quality_checker.wd_database import WdDatabase


def test_remove_database(tmpdir):
    temp_direcotry = tmpdir.mkdir("test_database_common")
    path_2_wd_db = temp_direcotry.join("wd.db")
    db = WdDatabase(path_2_wd_db)
    del db

    remove_existing_database(path_2_wd_db)

    assert os.path.exists(path_2_wd_db) is False


def test_do_database_exist(tmpdir):
    temp_direcotry = tmpdir.mkdir("test_database_common")
    path_2_wd_db = temp_direcotry.join("wd.db")
    db = WdDatabase(path_2_wd_db)
    del db

    assert do_databases_exist([path_2_wd_db])

    remove_existing_database(path_2_wd_db)

    assert do_databases_exist([path_2_wd_db]) is False
