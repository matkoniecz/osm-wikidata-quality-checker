from pytest_httpserver import HTTPServer

from osm_wikidata_quality_checker.downloader import download


def test_downloader(tmpdir, httpserver: HTTPServer):
    temp_direcotry = tmpdir.mkdir("downloader_test")
    file_content_response = "test content"
    httpserver.expect_request("/download/test.csv", method="GET").respond_with_data(
        file_content_response
    )
    path_to_downloaded_file = temp_direcotry.join("test.csv")
    download(httpserver.url_for("/download/test.csv"), path_to_downloaded_file, 0)

    with open(path_to_downloaded_file) as file:
        assert file_content_response == file.read()
