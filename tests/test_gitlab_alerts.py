import pytest
from pytest_httpserver import HTTPServer

from osm_wikidata_quality_checker.gitlab_alerts import (
    INSTANCE_NAME,
    Severity,
    send_alert,
)


def test_send_alert(
    httpserver: HTTPServer,
):
    import socket

    url, auth = f"http://{httpserver.host}:{httpserver.port}", "very-secret"

    title = "test_send_alert"
    description = "test_send_alert message"
    severity = Severity.High

    headers = {"Authorization": "Bearer " + auth}

    body = {
        "title": f"{INSTANCE_NAME}:{title}",
        "description": description,
        "severity": severity,
        "hosts": socket.gethostname(),
    }

    httpserver.expect_request(
        "/", headers=headers, method="POST", json=body
    ).respond_with_data("OK", status=999)

    assert send_alert(title, description, severity, url=url, auth=auth) == 999
