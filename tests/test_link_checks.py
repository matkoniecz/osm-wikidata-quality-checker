from shapely.geometry import Point

from osm_wikidata_quality_checker.checker_issues import (
    AreaLink,
    Fix,
    LivingOrganismIssue,
    NodeLink,
    PlaceMismatchIssue,
    PrimaryTagClaimMismatchIssue,
    SecondaryTagClaimMismatchIssue,
    UnpermittedInstanceIssue,
    VeryLargeDistanceIssue,
    create_link,
)
from osm_wikidata_quality_checker.link_checks import (
    check_living_organism,
    check_places,
    check_primary_tag_instance_of,
    check_secondary_tags_instance_of,
    check_unpermitted_instance,
    check_very_large_distance,
)
from osm_wikidata_quality_checker.osm_database import (
    Location,
    OsmArea,
    OsmNode,
    OsmRelation,
)
from osm_wikidata_quality_checker.wd_constants import (
    Q_ART_GROUP,
    Q_BRAND,
    Q_COMPANY,
    Q_HUMAN,
    Q_INDIVIDUAL_ORGANISM,
    Q_INDUSTRIAL_BUILDING,
    Q_LIST,
    Q_LIVING_ORGANISM,
    Q_MEMORIAL,
    Q_NATURAL_MONUMENT,
    Q_ORGANISMS_KNOWN_BY_A_PARTICULAR_COMMON_NAME,
    Q_ORGANIZATION,
    Q_TAXON,
    Q_TREE,
    Q_WIKIMEDIA_DISAMBIGUATION_PAGE,
)
from osm_wikidata_quality_checker.wd_database import WdCoordinate, WdItem


def test_check_unpermitted_instance_direct(osmNode: OsmNode, wdItem: WdItem):
    link = NodeLink(osmNode, wdItem, "wikidata")

    link.wd.all_instances_and_subclasses = set()

    link.wd.instances_of = {Q_WIKIMEDIA_DISAMBIGUATION_PAGE}
    issue = check_unpermitted_instance(link)
    assert isinstance(issue, UnpermittedInstanceIssue)
    assert issue.unpermitted_instance_qid == Q_WIKIMEDIA_DISAMBIGUATION_PAGE

    link.wd.instances_of = {Q_LIST}
    issue = check_unpermitted_instance(link)
    assert isinstance(issue, UnpermittedInstanceIssue)
    assert issue.unpermitted_instance_qid == Q_LIST


def test_check_living_organism_positive(osmNode: OsmNode, wdItem: WdItem):
    link = NodeLink(osmNode, wdItem, "wikidata")
    link.wd.instances_of = set()

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM,)
    issue = check_living_organism(link)
    assert isinstance(issue, LivingOrganismIssue)

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM, Q_TREE)
    issue = check_living_organism(link)
    assert isinstance(issue, LivingOrganismIssue)

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM, Q_TAXON)
    link.wd.instances_of = (Q_TREE,)
    issue = check_living_organism(link)
    assert isinstance(issue, LivingOrganismIssue)
    link.wd.instances_of = set()

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (
        Q_LIVING_ORGANISM,
        Q_ORGANISMS_KNOWN_BY_A_PARTICULAR_COMMON_NAME,
    )
    link.wd.instances_of = (Q_TREE,)
    issue = check_living_organism(link)
    assert isinstance(issue, LivingOrganismIssue)
    link.wd.instances_of = set()


def test_check_living_organism_negative(osmNode: OsmNode, wdItem: WdItem):
    link = NodeLink(osmNode, wdItem, "wikidata")
    link.wd.instances_of = set()

    link.key = "subject:wikidata"
    link.wd.all_instances_and_subclasses = {Q_TREE, Q_INDIVIDUAL_ORGANISM}
    issue = check_living_organism(link)
    assert issue is None

    link.key = "subject:wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM, Q_HUMAN)
    issue = check_living_organism(link)
    assert issue is None

    link.wd.instances_of = {Q_HUMAN, Q_COMPANY}
    link.wd.all_instances_and_subclasses = {Q_LIVING_ORGANISM, Q_HUMAN, Q_COMPANY}
    issue = check_living_organism(link)
    assert issue is None

    link.key = "subject:wikidata"
    link.wd.all_instances_and_subclasses = (
        Q_LIVING_ORGANISM,
        Q_TREE,
        Q_INDIVIDUAL_ORGANISM,
    )
    issue = check_living_organism(link)
    assert issue is None

    link.key = "operator:wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM, Q_HUMAN)
    link.wd.instances_of = {Q_HUMAN}
    issue = check_living_organism(link)
    assert issue is None
    link.wd.instances_of = set()

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM, Q_INDIVIDUAL_ORGANISM)
    issue = check_living_organism(link)
    assert issue is None

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM, Q_NATURAL_MONUMENT)
    issue = check_living_organism(link)
    assert issue is None

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM, Q_MEMORIAL)
    issue = check_living_organism(link)
    assert issue is None

    link.key = "wikidata"
    link.wd.all_instances_and_subclasses = (Q_LIVING_ORGANISM,)
    link.wd.instances_of = (Q_TREE,)
    issue = check_living_organism(link)
    assert issue is None
    link.wd.instances_of = set()


def test_check_secondary_wikidata_tags_empty_instances():
    node = OsmNode()
    node.oid = 1

    wd_item = WdItem()
    wd_item.qid = 2
    wd_item.all_instances_and_subclasses = set()
    link = NodeLink(node, wd_item, "brand:wikidata")

    issue = check_secondary_tags_instance_of(link)
    assert issue is None


def test_check_secondary_wikidata_tags_with_instance_match():
    node = OsmNode()
    node.oid = 1

    wd_item = WdItem()
    wd_item.qid = 2
    wd_item.all_instances_and_subclasses = {Q_BRAND}
    link = NodeLink(node, wd_item, "brand:wikidata")

    issue = check_secondary_tags_instance_of(link)
    assert issue is None


def test_check_secondary_wikidata_tags_with_primary():
    node = OsmNode()
    node.oid = 1

    wd_item = WdItem()
    wd_item.qid = 2
    wd_item.all_instances_and_subclasses = {Q_BRAND}
    link = NodeLink(node, wd_item, "wikidata")

    issue = check_secondary_tags_instance_of(link)
    assert issue is None


def test_check_secondary_wikidata_tags_with_no_instance_match():
    node = OsmNode()
    node.oid = 1

    wd_item = WdItem()
    wd_item.qid = 2
    wd_item.all_instances_and_subclasses = {Q_HUMAN, Q_ART_GROUP}
    link = NodeLink(node, wd_item, "brand:wikidata")

    issue = check_secondary_tags_instance_of(link)
    assert isinstance(issue, SecondaryTagClaimMismatchIssue)


def test_check_secondary_wikidata_tags_with_ignored_key():
    node = OsmNode()
    node.oid = 1

    wd_item = WdItem()
    wd_item.qid = 2
    wd_item.all_instances_and_subclasses = {Q_HUMAN, Q_ART_GROUP}
    link = NodeLink(node, wd_item, "ignored_by_check:wikidata")

    issue = check_secondary_tags_instance_of(link)
    assert issue is None


def test_check_places_not_a_place():
    node = OsmNode()
    node.tags = {}
    wd = WdItem()
    link = NodeLink(node, wd, "wikidata")
    assert check_places(link) is None


def test_check_places_pass():

    node = OsmNode()
    node.tags = {"place": "village", "name": "test_check_places_node"}
    node._location = Location(1.1, 2.2)

    area = OsmArea()
    area.tags = {"place": "village", "name": "test_check_places_node"}
    area._shape = Point(1.1, 2.2)
    area._shape_created = True

    wd = WdItem()
    wd.names = {"xx": "test_check_places_node"}
    wd.coordinates = [WdCoordinate(1.1, 2.2)]

    node_link = NodeLink(node, wd, "wikidata")
    assert check_places(node_link) is None

    area_link = AreaLink(area, wd, "wikidata")
    assert check_places(area_link) is None


def test_check_places_fail():
    node = OsmNode()
    node.tags = {"place": "village", "name": "test_check_places_node"}
    node._location = Location(1.1, 2.2)

    area = OsmArea()
    area.tags = {"place": "village", "name": "test_check_places_node"}
    area._shape = Point(1.1, 2.2)
    area._shape_created = True

    wd = WdItem()
    wd.names = {"xx": "xxx"}
    wd.coordinates = [WdCoordinate(99.9, 99.9)]

    node_link = NodeLink(node, wd, "wikidata")
    assert isinstance(check_places(node_link), PlaceMismatchIssue)

    area_link = AreaLink(area, wd, "wikidata")
    assert isinstance(check_places(area_link), PlaceMismatchIssue)


def test_check_primary_tags():
    node = OsmNode()
    node.tags = {"industrial": "shipyard"}

    wd = WdItem()
    wd.qid = 1
    wd.names = {"xx": "test_check_primary_node"}
    wd.all_instances_and_subclasses = {1, 190928}  # > shipyard

    node_link = NodeLink(node, wd, "wikidata")
    assert check_primary_tag_instance_of(node_link) is None

    wd.all_instances_and_subclasses = {1, Q_INDUSTRIAL_BUILDING}
    assert check_primary_tag_instance_of(node_link) is None

    node.tags = {"test_xyz": "yes", "industrial": "xyz"}
    wd.all_instances_and_subclasses = {1, 190928, Q_INDUSTRIAL_BUILDING}
    assert check_primary_tag_instance_of(node_link) is None

    node.tags = {"test_xyz": "yes"}
    wd.all_instances_and_subclasses = {1, 190928, Q_INDUSTRIAL_BUILDING}
    assert check_primary_tag_instance_of(node_link) is None

    node_link = NodeLink(node, wd, "secondary:wikidata")
    assert check_primary_tag_instance_of(node_link) is None


def test_check_primary_tags_ignore():
    node = OsmNode()
    node.tags = {"industrial": "shipyard"}

    wd = WdItem()
    wd.qid = 190928  # > shipyard
    wd.names = {"xx": "test_check_primary_node"}
    wd.all_instances_and_subclasses = {190928}  # > shipyard

    node_link = NodeLink(node, wd, "wikidata")
    assert check_primary_tag_instance_of(node_link) is None


def test_check_primary_tags_fixes():
    node = OsmNode()
    node.tags = {"industrial": "shipyard"}

    wd = WdItem()
    wd.qid = 1
    wd.names = {"xx": "test_check_primary_node"}
    wd.all_instances_and_subclasses = {1, 123, 1234}

    node_link = NodeLink(node, wd, "wikidata")
    issue = check_primary_tag_instance_of(node_link)
    assert isinstance(issue, PrimaryTagClaimMismatchIssue)
    assert len(issue.fixes) == 1
    assert isinstance(issue.fixes[0], Fix)

    wd.all_instances_and_subclasses = {1, Q_ORGANIZATION}
    issue = check_primary_tag_instance_of(node_link)
    assert isinstance(issue, PrimaryTagClaimMismatchIssue)
    assert len(issue.fixes) == 1
    # assert len(issue.fixes) == 2
    assert isinstance(issue.fixes[0], Fix)
    # assert isinstance(issue.fixes[1], MultiFix)


def test_very_large_distance_point():
    for T in (OsmNode, OsmRelation):
        osm_obj = T()
        osm_obj.tags = {}
        osm_obj._location = Location(0.0, 0.0)
        wd = WdItem()
        wd.qid = 1
        link = create_link(osm_obj, wd, "wikidata")
        # positive
        wd.coordinates = [WdCoordinate(13, 13)]  # distance 2034km
        assert isinstance(check_very_large_distance(link), VeryLargeDistanceIssue)
        # negative
        wd.coordinates = [WdCoordinate(12, 12)]  # distance 1879 km
        assert check_very_large_distance(link) is None


def test_very_large_distance_shape():
    osm_obj = OsmArea()
    osm_obj.tags = {}
    osm_obj._shape = Point(0.0, 0.0)
    wd = WdItem()
    wd.qid = 1
    link = create_link(osm_obj, wd, "wikidata")
    # positive
    wd.coordinates = [WdCoordinate(13, 13)]  # distance 2034km
    assert isinstance(check_very_large_distance(link), VeryLargeDistanceIssue)
    # negative
    wd.coordinates = [WdCoordinate(12, 12)]  # distance 1879 km
    assert check_very_large_distance(link) is None


def test_distance_too_large_check_primary_only():
    node = OsmNode()
    node._location = Location(0.0, 0.0)

    wd = WdItem()
    wd.qid = 1
    wd.coordinates = [WdCoordinate(50, 50)]  # distance 7289km

    link = create_link(node, wd, "test:wikidata")
    assert check_very_large_distance(link) is None


def test_distance_too_large_check_tags():
    for tags in ({"water": "river"}, {"place": "continent"}, {"place": "ocean"}):
        node = OsmNode()
        node._location = Location(0.0, 0.0)
        wd = WdItem()
        wd.qid = 1
        wd.coordinates = [WdCoordinate(50, 50)]  # distance 7289km
        link = create_link(node, wd, "wikidata")
        # positive
        link.osm.tags = {}
        assert isinstance(check_very_large_distance(link), VeryLargeDistanceIssue)
        # negative
        link.osm.tags = tags
        assert check_very_large_distance(link) is None
