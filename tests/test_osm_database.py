# pylint: disable=redefined-outer-name
import random

import pytest
import shapely.geometry

from osm_wikidata_quality_checker.osm_database import (
    OsmArea,
    OsmDatabase,
    OsmNode,
    OsmRelation,
    OsmWay,
)


@pytest.fixture
def database():
    return OsmDatabase(":memory:")


@pytest.mark.filterwarnings("ignore::RuntimeWarning")
def test_primary_key_ignore(database: OsmDatabase):
    node1 = OsmNode()
    node1.oid = 1
    node2 = OsmNode()
    node2.oid = 1

    database.add_all([node1, node2])

    node3 = OsmNode()
    node3.oid = 1

    database.add_all([node3])


def test_get_all(database: OsmDatabase):
    nodes = []
    for i in range(100, 110):
        node = OsmNode()
        node.oid = i
        nodes.append(node)

    database.add_all(nodes)
    db_nodes = [item for sub_list in database.get_all(100) for item in sub_list]
    assert len(db_nodes) == len(nodes)
    assert all(a.oid == b for a, b in zip(db_nodes, range(100, 110)))


def test_get_all_type(database: OsmDatabase):
    nodes = []
    ways = []
    for i in range(100, 110):
        node = OsmNode()
        node.oid = i
        nodes.append(node)
        way = OsmWay()
        way.oid = i * 30
        ways.append(way)

    database.add_all(nodes + ways)

    db_nodes = [
        item for sub_list in database.get_all_type(OsmNode, 100) for item in sub_list
    ]
    assert len(db_nodes) == len(nodes)
    assert all(isinstance(n, OsmNode) for n in db_nodes)
    assert all(a.oid == b for a, b in zip(db_nodes, range(100, 110)))

    db_ways = [
        item for sub_list in database.get_all_type(OsmWay, 100) for item in sub_list
    ]
    assert len(db_ways) == len(ways)
    assert all(isinstance(w, OsmWay) for w in db_ways)
    assert all(a.oid == b * 30 for a, b in zip(db_ways, range(100, 110)))


def test_get_node(database: OsmDatabase):
    import random

    expected_id = random.randint(1, 100)
    node = OsmNode()
    node.oid = expected_id
    database.add_all([node])
    actual_id = database.get_node(expected_id).oid
    assert actual_id == expected_id


def test_get_way(database: OsmDatabase):
    import random

    expected_id = random.randint(1, 100)
    way = OsmWay()
    way.oid = expected_id
    database.add_all([way])
    actual_id = database.get_way(expected_id).oid
    assert actual_id == expected_id


def test_get_relation(database: OsmDatabase):
    expected_id = random.randint(1, 100)
    relation = OsmRelation()
    relation.oid = expected_id
    relation.members = []
    database.add_all([relation])
    actual_id = database.get_relation(expected_id).oid
    assert actual_id == expected_id


def test_get_area(database: OsmDatabase):
    expected_id = random.randint(1, 100)
    area = OsmArea()
    area.uid = expected_id
    database.add_all([area])
    actual_id = database.get_area(expected_id).uid
    assert actual_id == expected_id
