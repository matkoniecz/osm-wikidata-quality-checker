import logging
from datetime import datetime
from time import perf_counter

import pytest

from osm_wikidata_quality_checker.osm_database import OsmDatabase
from osm_wikidata_quality_checker.osm_import import (
    _build_osmose_country_config,
    _check_all_countries_imported,
    osm_import,
)

# LOGGER = logging.getLogger(__name__)


test_osm_import_MAX_TIME = 20


def test_osmose_country_config_negative(caplog):
    test_country_config = "./tests/res/test_osmose_country_config.json"

    # test buld
    config = _build_osmose_country_config(test_country_config)
    assert len(config) == 1

    # test assert not in db
    country_oid_in_db = set()
    # Example: "WARNING  root:osm_import.py:46 osmose country test_test (rel=1234 / country_code=TE-1) was not found during import of the osm dump\n"

    with caplog.at_level(logging.WARNING):
        _check_all_countries_imported(country_oid_in_db, config)
        assert caplog.text != ""
        assert str(caplog.text).startswith("WARNING  root:osm_import.py:")
        assert str(caplog.text).endswith(
            "osmose country test_test (rel=1234 / country_code=TE-1) is not in db\n"
        )


def test_osmose_country_config_positive(caplog):
    test_country_config = "./tests/res/test_osmose_country_config.json"

    # test buld
    config = _build_osmose_country_config(test_country_config)
    assert len(config) == 1

    # test assert in db
    country_oid_in_db = set("1234")
    config = dict()
    with caplog.at_level(logging.WARNING):
        _check_all_countries_imported(country_oid_in_db, config)
    assert caplog.text == ""


@pytest.mark.slow
@pytest.mark.timeout(test_osm_import_MAX_TIME * 3)
def test_osm_import(tmpdir):
    osm_test_dump = "./tests/dumps/andorra-latest.osm.pbf"
    osm_test_db = tmpdir.join("osm_test.db")
    runtime_log = "./tests/dumps/osm_import_runtime.log"

    start = perf_counter()
    osm_import(osm_test_db, osm_test_dump, 100, False, False)
    import_time_s = perf_counter() - start

    with open(runtime_log, mode="a", encoding="utf-8") as file:
        file.write(f"{datetime.now()}\t{osm_test_dump}\t{import_time_s}s\n")

    db = OsmDatabase(osm_test_db)

    assert (
        import_time_s < test_osm_import_MAX_TIME
    ), f"runtime of osm import was {import_time_s}s (should be lower than {test_osm_import_MAX_TIME}s)"
    assert db.get_node(58957648)  # Node: Andorra la Vella (58957648)
    assert db.get_area(2 * 3657693 + 1)  # Relation: Andorra la Vella (3657693)
