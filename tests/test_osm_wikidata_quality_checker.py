from osm_wikidata_quality_checker import __version__ as v_init
from osm_wikidata_quality_checker import main


def test_version_init():
    assert v_init == "0.1.0"
