import os

import pytest
from pytest_httpserver import HTTPServer, httpserver

from osm_wikidata_quality_checker.checker_issues import RedirectedWdItemIssue
from osm_wikidata_quality_checker.osmose_reporter import (
    _ISSUE_2_OSMOSE_CLASS_MAPPING,
    InvalidWdItemFormatIssue,
    OsmoseErrorDetails,
    OsmoseReporter,
    _CountryAnalyser,
    _get_osmose_class,
)

# from http.server import HTTPServer


def test_unique_class_id():
    assert (
        len(_ISSUE_2_OSMOSE_CLASS_MAPPING) != 0
    ), "issue to osmose class mapping missing"
    ids = [v.id for v in _ISSUE_2_OSMOSE_CLASS_MAPPING.values()]
    assert len(ids) == len(set(ids)), "osmose class id's are not unique"


def test_get_osmose_class(invalidWdItemFormatIssue, redirectedWdItemIssue):
    assert isinstance(
        _get_osmose_class(invalidWdItemFormatIssue), OsmoseErrorDetails
    ), f"class actual {InvalidWdItemFormatIssue} / class shuld {OsmoseErrorDetails}"

    assert isinstance(
        _get_osmose_class(redirectedWdItemIssue), OsmoseErrorDetails
    ), f"class actual {RedirectedWdItemIssue} / class shuld {OsmoseErrorDetails}"


@pytest.fixture
def reporter(tmpdir, httpserver: HTTPServer):
    new_reporter = OsmoseReporter(
        tmpdir,
        [],
        api_host=f"http://{httpserver.host}:{httpserver.port}",
    )
    new_reporter.open()
    p = new_reporter.path_2_reports_folder
    yield new_reporter


def test_send_xml(
    reporter: OsmoseReporter,
    invalidWdItemFormatIssue,
    redirectedWdItemIssue,
    httpserver: HTTPServer,
):
    reporter._country_analysers.append(_CountryAnalyser(1, "TST", "Test", None))
    reporter._country_analysers[0].issues.append(invalidWdItemFormatIssue)
    reporter._country_analysers[0].issues.append(redirectedWdItemIssue)

    httpserver.expect_request("/control/send-update", method="POST").respond_with_data(
        "OK", status=200
    )

    assert reporter._send_reports()
