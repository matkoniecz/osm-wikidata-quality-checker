import csv

import pytest
import xlrd

from osm_wikidata_quality_checker.checker_issues import (
    CheckerIssue,
    InvalidWdItemFormatIssue,
    RedirectedWdItemIssue,
)
from osm_wikidata_quality_checker.osm_database import OsmNode
from osm_wikidata_quality_checker.reporter import CsvReporter, ExcelReporter


def test_csv_reporter(
    tmpdir, invalidWdItemFormatIssue: CheckerIssue, redirectedWdItemIssue: CheckerIssue
):
    reporter = CsvReporter(tmpdir.mkdir("reports"), "csv_reporter_test.csv")
    reporter.open()
    reporter.report([invalidWdItemFormatIssue, redirectedWdItemIssue])
    reporter.flush()
    reporter.close()

    with open(reporter.path_2_report, "r", encoding="utf8") as file:
        csv_file = csv.reader(file)
        lines = [l for l in csv_file]
        assert len(lines) == 3

        issue_line = lines[1]
        assert int(issue_line[2]) == invalidWdItemFormatIssue.osm.oid
        assert int(issue_line[4]) == invalidWdItemFormatIssue.qid

        issue_line = lines[2]
        assert int(issue_line[2]) == redirectedWdItemIssue.osm.oid
        assert int(issue_line[4]) == redirectedWdItemIssue.qid


def test_excel_reporter(
    tmpdir, invalidWdItemFormatIssue: CheckerIssue, redirectedWdItemIssue: CheckerIssue
):
    filename = "test_excel_reporter.xlsx"
    reporter = ExcelReporter(tmpdir, filename)
    # reporter = ExcelReporter("./tests", filename)  # DEBUG only
    reporter.open()
    reporter.report([invalidWdItemFormatIssue, redirectedWdItemIssue])
    reporter.flush()
    reporter.close()

    with xlrd.open_workbook(reporter.path_2_report) as wb:
        assert wb.sheet_names()[0] == "issues"
        ws = wb.sheet_by_index(0)
        assert ws.nrows == 7
