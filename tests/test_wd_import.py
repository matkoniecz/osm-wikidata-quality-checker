from datetime import datetime
from time import perf_counter

import pytest

from osm_wikidata_quality_checker.wd_constants import (
    P_COORDINATE_LOCATION,
    P_INSTANCE_OF,
    P_SUBCLASS_OF,
)
from osm_wikidata_quality_checker.wd_database import WdDatabase, WdItem
from osm_wikidata_quality_checker.wd_import import (
    _create_item,
    _get_fist_numeric_id_of_claim,
    _import_function,
    wd_import,
)


@pytest.fixture
def wd_entity():
    return {
        "id": "Q1",
        "labels": {
            "de": {"value": "test entity"},
            "en": {"value": "test entity en"},
        },
        "claims": {
            P_COORDINATE_LOCATION: [
                {"mainsnak": {"datavalue": {"value": {"latitude": 1, "longitude": 2}}}},
                {"mainsnak": {"datavalue": {"value": {"latitude": 3, "longitude": 4}}}},
            ],
            P_INSTANCE_OF: [
                {"mainsnak": {"datavalue": {"value": {"numeric-id": 1}}}},
                {"mainsnak": {"datavalue": {"value": {"numeric-id": 2}}}},
            ],
            P_SUBCLASS_OF: [
                {"mainsnak": {"datavalue": {"value": {"numeric-id": 3}}}},
                {"mainsnak": {"datavalue": {"value": {"numeric-id": 4}}}},
            ],
            "ID_TO_TEST": [
                {"mainsnak": {"datavalue": {"value": {"numeric-id": 1}}}},
            ],
        },
    }


def test_create_item(wd_entity):
    wd_item = _create_item(wd_entity)
    assert wd_item.qid == 1

    assert wd_item.coordinates[0].lat == 1
    assert wd_item.coordinates[0].lon == 2
    assert wd_item.coordinates[1].lat == 3
    assert wd_item.coordinates[1].lon == 4

    assert len(wd_item.instances_of & set({1, 2})) == 2
    assert len(wd_item.subclasses_of & set({3, 4})) == 2


def test_get_fist_numeric_id_of_claim(wd_entity):
    no_id = _get_fist_numeric_id_of_claim(wd_entity, "NO_Q_ID")
    assert no_id is None

    multiple_ids = _get_fist_numeric_id_of_claim(wd_entity, P_INSTANCE_OF)
    assert multiple_ids == -1

    valid_id = _get_fist_numeric_id_of_claim(wd_entity, "ID_TO_TEST")
    assert valid_id == 1


test_wd_import_MAX_TIME = 6


@pytest.mark.slow
@pytest.mark.timeout(test_wd_import_MAX_TIME * 3)
def test_wd_import(tmpdir):
    wd_test_dump = "./tests/dumps/wd_test_dump.json.bz2"
    wd_test_db = tmpdir.join("wd_test.db")
    # wd_test_db = "./tests/dumps/wd_test.db"
    runtime_log = "./tests/dumps/wd_import_runtime.log"

    start = perf_counter()
    wd_import(wd_test_db, wd_test_dump, 10, False)
    import_time_s = perf_counter() - start

    with open(runtime_log, mode="a") as file:
        file.write(f"{datetime.now()}\t{wd_test_dump}\t{import_time_s}s\n")

    db = WdDatabase(wd_test_db)

    assert (
        import_time_s < test_wd_import_MAX_TIME
    ), f"runtime of wd import was {import_time_s}s (should be lower than {test_wd_import_MAX_TIME}s)"
    assert db.get(233)  # Malta (Q233)
