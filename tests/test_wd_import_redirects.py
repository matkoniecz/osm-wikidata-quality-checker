import pytest

from osm_wikidata_quality_checker.wd_redirects_database import WdRedirectsDatabase
from osm_wikidata_quality_checker.wd_redirects_import import (
    _import_function,
    redirects_import,
)


@pytest.fixture()
def qredirects(tmpdir):
    path = tmpdir.join("test_qredirects.csv")
    with open(path, "wt") as f:
        f.write(
            """RedirectFrom,RedirectTo,LastModified
L273182,L38248,2020-03-20T10:15:31Z
Q65404786,Q1191,2020-08-19T04:23:19Z
Q65423687,Q1208,2020-01-15T08:13:52Z
Q65425945,Q1226,2020-11-24T05:37:52Z
Q65425984,Q1232,2020-11-24T06:40:10Z
Q65429373,Q1218,2020-05-06T02:29:20Z"""
        )
    return path


def test_import_function(qredirects):

    actual = []
    _import_function(qredirects, actual.append)

    assert len(actual) == 5
    assert actual[0].qid_from == 65404786
    assert actual[0].qid_to == 1191
    assert actual[4].qid_from == 65429373
    assert actual[4].qid_to == 1218


def test_redirects_import(tmpdir, qredirects):
    path_2_db = tmpdir.join("test_db.db")  # HINT :memory: does not work
    db = WdRedirectsDatabase(path_2_db)
    redirects_import(path_2_db, qredirects, 3, False)

    L273182 = db.get(273182)
    Q65404786 = db.get(65404786)

    assert not L273182
    assert Q65404786


def test_redirects_import_mp(tmpdir, qredirects):

    path_2_db = tmpdir.join("test_db.db")  # HINT :memory: does not work

    redirects_import(path_2_db, qredirects, 3, True)

    db = WdRedirectsDatabase(path_2_db)

    L273182 = db.get(273182)
    Q65404786 = db.get(65404786)

    assert not L273182
    assert Q65404786
